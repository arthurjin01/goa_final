package com.example.goafinalproject;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

class EventAdapter extends RecyclerView.Adapter<EventsViewHolder> {

    // Constructor
    EventAdapter(@NonNull Context context, @NonNull ArrayList<Events> eventsArrayList, @NonNull MainActivity mainActivity) {
        this.context = context;
        this.eventArrayList = eventsArrayList;
        this.mainActivity = mainActivity;

    }
    // Overridden methods
    @NonNull
    @Override
    public EventsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        // Method called whenever new ViewHolder needs to be created
        LayoutInflater inflater = LayoutInflater.from(context);
        View itemView = inflater.inflate(R.layout.event_recycler_view, parent, false);
        EventsViewHolder viewHolder = new EventsViewHolder(itemView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull EventsViewHolder holder, int position) {
        // Method called whenever existing ViewHolder needs to be reused
        // Repopulate ViewHolder
        Events events = eventArrayList.get(position);
        holder.eventName.setText(events.eventName);
        holder.eventDate.setText(events.eventDate);
        holder.eventLocation.setText(events.eventLocation);
    }

    @Override
    public int getItemCount() {
        // Called whenever RecyclerView needs to know total items to display
        return eventArrayList.size();
    }


    // Properties
    Context context;
    ArrayList<Events> eventArrayList;
    MainActivity mainActivity;
}