package com.example.goafinalproject;

import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.goafinalproject.R;

import java.text.BreakIterator;

class EventsViewHolder extends RecyclerView.ViewHolder {

    // Constructor
    public EventsViewHolder(@NonNull View itemView) {
        super(itemView);

        this.itemView = itemView;
        eventName = itemView.findViewById(R.id.event_name);
        eventDate = itemView.findViewById(R.id.event_date);
        eventLocation = itemView.findViewById(R.id.event_location);

    }

    // Properties
    View itemView;
    TextView eventName;
    TextView eventDate;
    TextView eventLocation;
}
