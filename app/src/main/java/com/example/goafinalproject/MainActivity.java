package com.example.goafinalproject;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import androidx.appcompat.app.AppCompatActivity;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.example.goafinalproject.databinding.ActivityMainBinding;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    EditText communityName;
    String communityNameInput;

    private ActivityMainBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_home);

        connectXMLViews();
        populateDataModel();
        setupHomeRecyclerView();
        setupNotifsRecyclerView();

        //
        communityName = findViewById(R.id.community_name_input);

        binding = ActivityMainBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        BottomNavigationView navView = findViewById(R.id.nav_view);
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        AppBarConfiguration appBarConfiguration = new AppBarConfiguration.Builder(
                R.id.navigation_home, R.id.navigation_notifications, R.id.navigation_calendar, R.id.navigation_posts)
                .build();
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment_activity_main);
        NavigationUI.setupActionBarWithNavController(this, navController, appBarConfiguration);
        NavigationUI.setupWithNavController(binding.navView, navController);
    }

    public void store_input(View view) {
        communityNameInput = communityName.getText().toString();
    }

    void populateDataModel() {
        // Initialize properties of HomeEvents
        homeEventsList.todayEvents = new ArrayList<>();
        notifsEventsList.todayEvents = new ArrayList<>();

        // Create and initialize first event
        Events event = new Events();
        event.eventName = "GoA Finals";
        event.eventDate = "May 04 2021";
        event.eventLocation = "Zoom cloud meeting";

        // Add event to array of events in HomeEventsList
        homeEventsList.todayEvents.add(event);
        notifsEventsList.todayEvents.add(event);

        event = new Events();
        event.eventName = "Placeholder Event";
        event.eventDate = "May 27 2021";
        event.eventLocation = "Somewhere";
        homeEventsList.todayEvents.add(event);
        notifsEventsList.todayEvents.add(event);
    }

    void connectXMLViews() {
        homeEventsRecyclerView = findViewById(R.id.home_events_recycler_view);
        communityTextInput = findViewById(R.id.community_name_input);
        eventNameTextView = findViewById(R.id.event_name);
        eventDateTextView = findViewById(R.id.event_date);
        eventLocationTextView = findViewById(R.id.event_location);
        saveCommunityTextInput = findViewById(R.id.save_btn);
    }

    void setupHomeRecyclerView() {
        // Home linear layout manager
        LinearLayoutManager homeLayoutManager = new LinearLayoutManager(this);
        homeEventsRecyclerView.setLayoutManager(homeLayoutManager);

        // Connect adapter to recycler view

        // Home event adapter
        EventAdapter homeEventAdapter = new EventAdapter(this, homeEventsList.todayEvents, this);
        homeEventsRecyclerView.setAdapter(homeEventAdapter);
    }

    void setupNotifsRecyclerView() {
        // Notifs event adapter
        EventAdapter notifsEventAdapter = new EventAdapter(this, notifsEventsList.todayEvents, this);
        homeEventsRecyclerView.setAdapter(notifsEventAdapter);
    }

    // Properties
    EventList homeEventsList = new EventList();
    EventList notifsEventsList = new EventList();

    // Adapter for recycler views
    RecyclerView homeEventsRecyclerView;
    RecyclerView notifsEventsRecyclerView;

    // XML Views
    EditText communityTextInput;
    TextView eventNameTextView;
    TextView eventDateTextView;
    TextView eventLocationTextView;
    ImageButton saveCommunityTextInput;



}
