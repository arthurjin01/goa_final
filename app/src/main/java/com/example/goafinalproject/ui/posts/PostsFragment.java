package com.example.goafinalproject.ui.posts;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import com.example.goafinalproject.R;
import com.example.goafinalproject.databinding.FragmentPostsBinding;
import com.example.goafinalproject.ui.posts.PostsViewModel;

public class PostsFragment extends Fragment {

    private PostsViewModel postsViewModel;
    private FragmentPostsBinding binding;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        System.out.println("Creating view model");
        postsViewModel =
                new ViewModelProvider(this).get(PostsViewModel.class);

        System.out.println("Set binding");
        binding = FragmentPostsBinding.inflate(inflater, container, false);
        View root = binding.getRoot();

        System.out.println("Setting text view");
        // final TextView textView = binding.textPosts;
        postsViewModel.getText().observe(getViewLifecycleOwner(), new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
//                textView.setText(s);
            }
        });
        return root;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}